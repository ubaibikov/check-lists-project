<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1'], function () {

    /**
     * Auth
     */
    Route::post('register', 'Auth\AuthController@register');
    Route::post('login', 'Auth\AuthController@login')->name('login');


    Route::group(['middleware' => 'auth:api'], function () {

        /**
         * Logut | Get Auth User
         */
        Route::post('logout', 'Auth\AuthController@logout');
        Route::get('user', 'Auth\AuthController@getAuthUser');

        /**
         * Users
         */
        Route::group(['middleware' => 'banned'], function () {

            /**
             * @var $currentCheckList for were() method regular
             */
            $currentCheckList = ['userId' => '[0-9]+', 'checkListId' => '[0-9]+'];

            /**
             * @var $currentUser for were() method regular
             */
            $curentUser = ['userId' => '[0-9]+'];

            /**
             * Check List actions
             */
            Route::get('check-lists/{userId}', 'API\V1\CheckListController@get')->where($curentUser);
            Route::get('check-lists/{userId}/{checkListId}', 'API\V1\CheckListController@detail')->where($currentCheckList);
            Route::post('check-lists/{userId}', 'API\V1\CheckListController@create')->middleware('count-limit')->where($curentUser);
            Route::patch('check-lists/{userId}/{checkListId}', 'API\V1\CheckListController@update')->where($currentCheckList);
            Route::delete('check-lists/{userId}/{checkListId}', 'API\V1\CheckListController@delete')->where($curentUser);

            /**
             * Check List points
             */
            Route::get('check-lists/{userId}/{checkListId}/points', 'API\V1\PointController@get')->where($currentCheckList);
            Route::post('check-lists/{userId}/{checkListId}/points', 'API\V1\PointController@create')->where($currentCheckList);
            Route::get('check-lists/{userId}/{checkListId}/points/checks', 'API\V1\PointController@getCheckedPoints')->where($currentCheckList);
            Route::get('check-lists/{userId}/{checkListId}/points/unchecks', 'API\V1\PointController@getUncechekedPoints')->where($currentCheckList);

            /**
             * because for PATCH DELETE methods we need only Point identifier
             */
            Route::patch('check-lists/points/{pointId}', 'API\V1\PointController@update');
            Route::delete('check-lists/points/{pointId}', 'API\V1\PointController@delete');
        });
    });
});
