<?php

use Illuminate\Support\Facades\Route;



Route::get('usman', function () {
    return 'livak';
});

/**
 * Main Admin or Moders...
 */
Route::group(['middleware' => 'admin.role'], function () {

    /**
     * Users Actions
     */
    Route::get('users','API\V1\Admin\AdminUserController@get');
    Route::patch('users/ban/{userId}', 'API\V1\Admin\AdminUserController@ban')->middleware('admin.banned');
    Route::patch('users/{userId}', 'API\V1\Admin\AdminUserController@updateUserRole');
});

/**
 * Main Admin
 */
Route::group(['middleware' => 'admin'], function () {

    /**
     * User CheckList Count Actions
     */
    Route::get('check-lists/count/{userId}', 'API\V1\Admin\AdminCheckListUserController@get');
    Route::post('check-lists/count/{userId}', 'API\V1\Admin\AdminCheckListUserController@createOrUpdate');
});
