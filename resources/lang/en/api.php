<?php

return [

    /**
     * Points Lang
     */
    'point-cr' => 'Point created',
    'point-up' => 'Point updated',
    'point-dl' => 'Point deleted',

    /**
     * Points warning Lang
     */
    'point-nf' => 'Point not found',
    'point-w-dl' => 'Point deleted not found',

    /**
     * Check lists Lang
     */
    'check-list-cr' => 'Check list created',
    'check-list-up' => 'Check list updated',
    'check-list-dl' => 'Check list deleted',

    /**
     * Check lists warning Lang
     */
    'check-list-nf' => 'Check list not found',
    'check-list-w-cr' => 'Check list not created',
    'check-list-w-up' => 'Check list not updated',
    'check-list-w-dl' => 'Check list deleted not found',

    /**
     * Check lists count Lang
     */
    'check-list-count-cr' => 'Check list count created',
    'check-list-count-up' => 'Check list count updated',
    'check-list-count-dl' => 'Check list count deleted',

    /**
     * Check lists count warning Lang
     */
    'check-list-count-w-cr' => 'Check list count not created',
    'check-list-count-w-up' => 'Check list count not updated',
    'check-list-count-w-dl' => 'Check list count not deleted',

    /**
     * Users Lang
     */
    'user-ban' => 'User banned',
    'user-unban' => 'User unbanned',
    'user-role-up' => 'User role updated',

    /**
     * Users ban warning lang
     */
    'user-ban-w-isYou' => 'You are trying to ban yourself'
];
