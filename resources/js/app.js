require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './router';
import Index from './Index';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Auth  from './auth';
axios.defaults.baseURL = "http://127.0.0.1:8000/api/v1/";
window.Vue = Vue

window.auth = new Auth();

Vue.use(BootstrapVue)
Vue.use(VueRouter);
window.Event = new Vue;
const app = new Vue({
    router,
    el: '#app',
    components: {Index}
});
