import VueRouter from 'vue-router';

import Home from './components/pages/Home';
import Login from './components/pages/Login';
import Register from './components/pages/Register';
import Users from './components/pages/Users';
import CheckLists from './components/pages/CheckLists';
import PointLists from './components/pages/PointLists';

let routes = [
    {
        path: '/',
        component: Home,
        name: 'home'
    },
    {
        path: '/login',
        component: Login,
        meta: { auth: true },
    },
    {
        path: '/register',
        component: Register,
    },
    {
        path: '/users',
        component: Users,
        meta: { authAdmin: true }
    },
    {
        path: '/check-lists',
        component: CheckLists,
        meta: { user: true },
    },
    {
        path:'/check-lists/:id/point-lists',
        component: PointLists,
        meta: { user: true },
    }
];


const router = new VueRouter({
    routes,
    history: false
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.auth)) {
        if (auth.check()) {
            next({
                path: '/',
                query: { redirect: to.fullPath }
            })
        }
    }
    if (to.matched.some(record => record.meta.authAdmin)) {
        if (auth.check()) {
            if (!auth.isAdminOrModer()) {
                return router.push({ name: 'home' });
            }
        }
        if (!auth.check()) {
            return router.push({ name: 'home' });
        }
    }

    if (to.matched.some(record => record.meta.user)) {
        if (!auth.check()) {
            return router.push({ name: 'home' });
        }
    }
    next();
});

export default router;
