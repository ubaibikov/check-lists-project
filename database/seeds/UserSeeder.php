<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@mail.com',
            'role' => 'A',
            'password' => Hash::make('12345'),
        ]);

        DB::table('users')->insert([
            'name' => 'Moderator',
            'email' => 'moderator@mail.com',
            'role' => 'M',
            'password' => Hash::make('12345'),
        ]);

        DB::table('users')->insert([
            'name' => 'User',
            'email' => 'user@mail.com',
            'role' => 'U',
            'password' => Hash::make('12345'),
        ]);
    }
}
