<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const MAIN_ADMIN = 'A';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get Auth role A|M|U
     *
     * @return string
     */
    public function getAuthRole(): string
    {
        return $this->role;
    }

    /**
     * Check user is main admin
     *
     * @return string
     */
    public function isMainAdmin(): bool
    {
        return $this->role === self::MAIN_ADMIN;
    }
}
