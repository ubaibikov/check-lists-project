<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class DefaultUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'default:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Shows default users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all()->take(3);

        foreach($users as $user){
            $this->info("Email: $user->email, Name: $user->name, Password: 12345 ");
        }
    }
}
