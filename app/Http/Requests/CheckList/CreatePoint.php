<?php

namespace App\Http\Requests\CheckList;

use App\Http\Requests\ApiRequests;

class CreatePoint extends ApiRequests
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pointTask' => 'required|max:255'
        ];
    }
}
