<?php

namespace App\Http\Requests\CheckList;

use App\Http\Requests\ApiRequests;

class UpdatePoint extends ApiRequests
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_performed' => 'required|max:1'
        ];
    }
}
