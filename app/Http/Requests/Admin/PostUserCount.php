<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\ApiRequests;

class PostUserCount extends ApiRequests
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'count' => 'required|int',
        ];
    }
}
