<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\ApiRequests;

class RoleUpdate extends ApiRequests
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role' => 'required|string|max:1',
        ];
    }
}
