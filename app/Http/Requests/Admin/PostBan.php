<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\ApiRequests;

class PostBan extends ApiRequests
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ban' => 'required|int|max:1',
        ];
    }
}
