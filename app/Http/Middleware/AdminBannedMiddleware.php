<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Check Banned or not Who will ban|unban
 */
class AdminBannedMiddleware
{
    use \App\Services\Traits\ApiResponse;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->userId == auth()->user()->id){
            return $this->errorResponse(__('api.user-ban-w-isYou'));
        }
        return $next($request);
    }
}
