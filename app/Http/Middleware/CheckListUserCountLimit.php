<?php

namespace App\Http\Middleware;

use App\CheckList;
use App\CheckListUserCount;
use Closure;

class CheckListUserCountLimit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = auth()->user()->id;
        if (!$this->achievedCheckListCountLimit($userId)) {
            return response()->json(['You achieved limit'], 400);
        }

        return $next($request);
    }

    /**
     * Check achieved check List Count limit
     *
     * @param int $userID User identifier
     * @return bool
     */
    protected function achievedCheckListCountLimit(int $userId)
    {
        $checkList = CheckList::where('user_id',$userId)->get();

        if($checkList->isNotEmpty()){
            $checkListCountLimit = CheckListUserCount::where('user_id',$userId)->select('count')->first();
            if($checkListCountLimit) {
                if($checkList->count() <= (int)$checkListCountLimit->count){
                    return true;
                }else{
                    return false;
                }
            }
        }

        return true;
    }
}
