<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\HttpException;
class CheckUserIsBanned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->isBannedUser()){
            return response()->json([
                'errors' => 'sdjfndkjnjd',
            ], 422);
        }
        return $next($request);
    }

    public function isBannedUser(): bool
    {
        $user = auth()->user();
        return $user->has_ban ?? false;
    }
}
