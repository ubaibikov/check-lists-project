<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = auth()->user()->getAuthRole();

        if ($role == 'A' || $role == 'M') {
            return $next($request);
        }

        return response()->json([
            'message' => 'Your is not Admin',
        ], 401);
    }
}
