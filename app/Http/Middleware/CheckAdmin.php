<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdmin
{
    protected $responseJson;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authorizationRole = auth()->user();

        if($authorizationRole->isMainAdmin()){
            return $next($request);
        }

        return response()->json('err',400);

    }
}
