<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\Traits\ApiResponse;

abstract class ApiController extends Controller
{
    use ApiResponse;
    private $repository;
}
