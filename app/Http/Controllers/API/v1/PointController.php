<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\ApiController;
use App\Http\Requests\CheckList\CreatePoint;
use App\Http\Requests\CheckList\UpdatePoint;
use App\Services\Contracts\PointContract;

class PointController extends ApiController
{
    public function __construct(PointContract $pointRepository)
    {
        $this->repository = $pointRepository;
    }

    /**
     * Get Check list points
     *
     * @param int $userId User Identifier
     * @param int $checkListId Check list identifier
     * @return json Json data
     */
    public function get(int $userId, int $checkListId)
    {
        $getPoints = $this->repository->allPoints($userId, $checkListId);

        return $this->successResponse('OK', $getPoints, 200);
    }

    /**
     * Get checked | performed check list points
     *
     * @param int $userId User Identifier
     * @param int $checkListId Check list identifier
     * @return json Json data
     */
    public function getCheckedPoints(int $userId, int $checkListId)
    {
        $getCheckedPoints = $this->repository->getCheckedPoints($userId, $checkListId);

        return $this->successResponse('OK', $getCheckedPoints, 200);
    }

    /**
     * Get unchecked | performed check list points
     *
     * @param int $userId User Identifier
     * @param int $checkListId Check list identifier
     * @return json Json data
     */
    public function getUncechekedPoints(int $userId, int $checkListId)
    {
        $getUncechekedPoints = $this->repository->getUncheckedPoints($userId, $checkListId);

        return $this->successResponse('OK', $getUncechekedPoints, 200);
    }

    /**
     * Create Check list point
     *
     * @param int $userId User Identifier
     * @param int $checkListId Check list identifier
     * @param CreateCheckListPoint $request CreateCheckListPoint validate request
     * @return json Json data
     */
    public function create(int $userId, int $checkListId, CreatePoint $request)
    {
        $createCheckListPoint = $this->repository->savePoint($userId, $checkListId, ['pointTask' => $request->pointTask]);

        return $this->successResponse(__('api.point-cr'), null, 201);
    }

    /**
     * Update Check list point
     *
     * @param int $pointId Point Identifier
     * @param UpdateCheckListPoint $request UpdateCheckListPoint validate request
     * @return json Json data
     */
    public function update(int $pointId, UpdatePoint $request)
    {
        $updateCheckListPoint = $this->repository->updatePoint($pointId, ['is_performed' => $request->is_performed]);

        if (!$updateCheckListPoint) {
            return $this->errorResponse(__('api.point-w-up'), null, 404);
        }

        return $this->successResponse(__('api.point-up'), $updateCheckListPoint);
    }

    /**
     * Delete Check list point
     *
     * @param int $pointId Point Identifier
     * @return json Json data
     */
    public function delete(int $pointId)
    {
        $deleteCheckListPoint = $this->repository->deletePoit($pointId);

        if (!$deleteCheckListPoint) {
            return $this->errorResponse(__('api.point-w-dl'), null, 404);
        }

        return $this->successResponse(__('api.point-dl'));
    }
}
