<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\ApiController;
use App\Http\Requests\CheckList\CreateCheckList;
use App\Http\Requests\CheckList\UpdateCheckList;
use App\Services\Contracts\CheckListContract;

class CheckListController extends ApiController
{

    public function __construct(CheckListContract $checkListRepository)
    {
        $this->repository = $checkListRepository;
    }

    /**
     * Get Check lists where user
     *
     * @param int $userId user Indentifier
     * @return json Json data
     */
    public function get(int $userId)
    {
        $getCheckLists = $this->repository->all($userId);

        return $this->successResponse('OK', $getCheckLists, 200);
    }

    /**
     * Get detail Check list
     *
     * @param int $userId user Indentifier
     * @param int Check list identifier
     * @return json Json data
     */
    public function detail(int $userId, int $checkListId) //if need check-list name ....
    {
        $detailCheckList = $this->repository->detail($userId, $checkListId);

        return $this->successResponse('OK', $detailCheckList);
    }

    /**
     * Create Check list where user
     *
     * @param int $userId User identifier
     * @param CreateCheckList $request CreateCheckList validate request
     * @return json Json data
     */
    public function create(int $userId, CreateCheckList $request)
    {
        $createCheckList = $this->repository->save($userId, ['title' => $request->title]);

        return $this->successResponse(__('api.check-list-cr'), null, 201);
    }

    /**
     * Update Check list where user
     *
     * @param int $userId User identifier
     * @param int $checkListId Check list identifier
     * @param UpdateCheckList $request UpdateCheckList vaslidate request
     * @return json Json data
     */
    public function update(int $userId, int $checkListId, UpdateCheckList $request)
    {
        $updateCheckList = $this->repository->update($userId, ['title' => $request->title]);

        return $this->successResponse(__('api.check-list-up'));
    }

    /**
     * Delete Check list where user
     *
     * @param int $userId User identifier
     * @param int $checkListId Check list identifier
     * @return json Json data
     */
    public function delete(int $userId, int $checkListId)
    {
        $deleteUserCheckList = $this->repository->detailDestroy($userId, $checkListId);

        if (!$deleteUserCheckList) {
            return $this->errorResponse(__('api.check-list-w-dl'), null, 404);
        }

        return $this->successResponse(__('api.check-list-dl'),$deleteUserCheckList);
    }
}
