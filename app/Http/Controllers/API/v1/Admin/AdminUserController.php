<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\ApiController;
use App\User;
use App\Http\Requests\Admin\{RoleUpdate,PostBan};

class AdminUserController extends ApiController
{
    /**
     * Get users
     *
     * @return json Json Data
     */
    public function get()
    {
        $users = User::all();

        return $this->successResponse('OK', $users->toArray());
    }

    /**
     * Ban User
     *
     * @param int $userId User identifier
     * @return json Json data
     */
    public function ban(int $userId, PostBan $request)
    {
        $this->makeCurrentBan($userId,$request->ban);
        return $request->ban ? $this->successResponse(__('api.user-ban')) : $this->successResponse(__('api.user-unban'));
    }

    /**
     * Update user role
     *
     * @param int $userId User identifier
     * @param RoleUpdate $request RoleUpdate validate request
     * @return json Json data
     */
    public function updateUserRole(int $userId, RoleUpdate $request)
    {
        $updateUserRole = User::where('id', $userId)->update(['role' => $request->role]);

        return $this->successResponse(__('api.user-role-up'));
    }

    /**
     * Make current ban|unban
     *
     * @param int $userId User identifier
     * @param int $banned banned value
     * @return \App\User model
     */
    protected function makeCurrentBan(int $userId, int $banned)
    {
        return User::where('id', $userId)->update(['has_ban' => $banned]);
    }
}
