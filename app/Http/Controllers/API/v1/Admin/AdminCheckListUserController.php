<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\CheckListUserCount;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Admin\PostUserCount;

class AdminCheckListUserController extends ApiController
{

    /**
     * Get User check list count limit
     *
     * @param int $userId User indentifier
     * @return json Json data
     */
    public function get(int $userId)
    {
        $getCheckListUserCount = CheckListUserCount::where('user_id', $userId)->first();
        return $this->successResponse('OK', $getCheckListUserCount);
    }

    /**
     * Create User count limit
     *
     * @param int $userId User indentifier
     * @param PostUserCount $request PostUserCount validate request
     * @return json Json data
     */
    public function createOrUpdate(int $userId, PostUserCount $request)
    {
        $createUserCheckListCount = CheckListUserCount::updateOrCreate(['user_id' => $userId], ['count' => $request->count]);
        return $this->successResponse(__('api.check-list-count-up'), null, 201);
    }
}
