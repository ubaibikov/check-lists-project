<?php

namespace App\Providers;

use App\Repositories\PointRepository;
use App\Repositories\CheckListRepository;
use Illuminate\Support\ServiceProvider;
use App\Services\Contracts\CheckListContract;
use App\Services\Contracts\PointContract;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        /**
         * Check list Repositiory
         * @var \App\Repositories\CheckListRepository;
         */
        $this->app->bind(CheckListContract::class, CheckListRepository::class);

        /**
         * Check list point Repository
         * @var \App\Repositories\CheckListPointRepository;
         */
        $this->app->bind(PointContract::class, PointRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
