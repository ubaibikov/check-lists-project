<?php

namespace App\Providers;

use App\CheckList;
use Illuminate\Support\ServiceProvider;
use App\Services\ValidResponse;

class ControllerResponseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Services\Contracts\PostActions', function ($app) {
            return new ValidResponse(new \App\Services\ResponseJson);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
