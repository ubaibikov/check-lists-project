<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckList extends Model
{
    protected $fillable = [
        'user_id',
        'check_list_title',
    ];

    /**
     * @var \App\Point Connection
     */
    public function points()
    {
        return $this->hasMany('App\Point','check_list_id');
    }
}
