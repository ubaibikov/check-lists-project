<?php

namespace App\Repositories;

use App\Point;
use App\Services\Contracts\PointContract;
use App\Repositories\ModelRepository;

final class PointRepository extends ModelRepository implements PointContract
{

    public function __construct(Point $point)
    {
        $this->model = $point;
    }

    public function allPoints(int $userId, int $checkListId)
    {
        $points = $this->getPoints($userId, $checkListId)
            ->select($this->getColumns())
            ->get();

        return $points;
    }

    public function savePoint(int $userId, int $checkListId, array $data)
    {
        $savePoint = $this->model::insert([
            'user_id' => $userId,
            'check_list_id' => $checkListId,
            'point_task' => $data['pointTask'],
        ]);

        return $savePoint;
    }

    public function updatePoint(int $pointId, array $data)
    {
        $updatePoint = $this->detailPoint($pointId)->update([
            'is_performed' => $data['is_performed']
        ]);

        return $updatePoint;
    }

    public function deletePoit(int $pointId)
    {
        $deletePoint = $this->detailPoint($pointId)->delete();

        return $deletePoint;
    }

    public function getCheckedPoints(int $userId, int $checkListId)
    {
        return $this->getCurrentCheckPoints($userId, $checkListId, 1);
    }

    public function getUncheckedPoints(int $userId, int $checkListId)
    {
        return $this->getCurrentCheckPoints($userId, $checkListId, 0);
    }

    private function getCurrentCheckPoints(int $userId, int $checkListId, int $performed)
    {
        return $this->getPoints($userId, $checkListId)->where('is_performed', $performed)
            ->select($this->getColumns())
            ->get();
    }

    private function getPoints(int $userId, int $checkListId)
    {
        return $this->model::where('user_id', $userId)->where('check_list_id', $checkListId);
    }

    private function detailPoint(int $pointId)
    {
        return $this->model::where('id', $pointId);
    }

    private function getColumns()
    {
        $fields = ['id', 'check_list_id', 'user_id', 'point_task','is_performed'];

        return $fields;
    }
}
