<?php

namespace App\Repositories;

use App\CheckList;
use App\Services\Contracts\CheckListContract;
use App\Repositories\ModelRepository;

final class CheckListRepository extends ModelRepository implements CheckListContract
{
    public function __construct(CheckList $checkList)
    {
        $this->model = $checkList;
    }

    public function all(int $userId)
    {
        return $this->model::where('user_id', $userId)->select($this->getColumns())->get();
    }

    public function detail(int $userId, int $checkListId)
    {
        return $this->detailByUser($userId, $checkListId)
            ->select($this->getColumns())
            ->get()
            ->first();
    }

    public function save(int $userId, array $data)
    {
        $save = [
            'user_id' => $userId,
            'check_list_title' => $data['title'],
        ];

        return $this->model::insert($save);
    }

    public function update(int $userId, array $data)
    {
        $update = [
            'check_list_title' => $data['title']
        ];

        return $this->model::where('user_id', $userId)
            ->update($update);
    }

    public function destroy(int $userId, int $checkListId)
    {
        $deleteDep = $this->deletePoints($checkListId);
        $detailDestroy = $this->detailByUser($userId, $checkListId);

        return $detailDestroy->delete();
    }

    private function deletePoints($checkListId)
    {
        return $this->model::where('id', $checkListId)->first()->points()->delete();
    }

    private function detailByUser(int $userId, int $checkListId)
    {
        return $this->model::where('user_id', $userId)->where('id', $checkListId);
    }

    private function getColumns()
    {
        $columns = ['id', 'user_id', 'check_list_title'];
        return $columns;
    }
}
