<?php

namespace App\Services\Traits;

trait ApiResponse
{
    /**
     * Get Success Response
     *
     * @param string $message Success response message
     * @param mixed $data Success response data
     * @return mixed Json Data
     */
    protected function successResponse(string $message, $data = null, int $status = 200)
    {
        return response()->json(self::makeResponse($message, $data), $status);
    }

    /**
     * Get Error Response
     *
     * @param string $error Error response message
     * @param mixed $data Error response data
     * @return mixed Json Data
     */
    protected function errorResponse(string $error, $data = null, int $status = 400)
    {
        return response()->json([
            'errors' => $error,
            'response' => $data,
        ], $status);
    }

    /**
     * Make Response data
     *
     * @param string $message Response message
     * @param mixed $data Response data
     * @return array
     */
    private static function makeResponse(string $message, $data = null)
    {
        return [
            'message' => $message,
            'response' => $data,
        ];
    }
}
