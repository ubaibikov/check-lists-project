<?php

namespace App\Services\Contracts;

/**
 * Contract for Point model
 */
interface PointContract
{
    public function allPoints(int $userId, int $checkListId);
    public function savePoint(int $userId, int $checkListId, array $data);

    public function updatePoint(int $pointId, array $data);
    public function deletePoit(int $pointId);

    public function getCheckedPoints(int $userId, int $checkListId);
    public function getUncheckedPoints(int $userId,int $checkListId);
}
