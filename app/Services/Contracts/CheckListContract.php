<?php

namespace App\Services\Contracts;

/**
 * Contract for CheckList Model
 */
interface CheckListContract
{
    /**
     * Get all
     *
     * @param int $userId User idetifier
     */
    public function all(int $userId);

    /**
     * Get detail
     *
     * @param int $userId User idetifier
     * @param int $userId Checklist idetifier
     */
    public function detail(int $userId, int $checkListId);

    /**
     * Save checklist
     *
     * @param int $userId User idetifier
     * @param int $userId Checklist idetifier
     */
    public function save(int $userId,array $data);

    /**
     * Update Check List
     *
     * @param int $userId User idetifier
     * @param int $userId Checklist idetifier
     */
    public function update(int $userId,array $data);

    /**
     * Destroy | delete Check list
     *
     * @param int $userId User idetifier
     * @param int $userId Checklist idetifier
     */
    public function destroy(int $userId, int $checkListId);
}
