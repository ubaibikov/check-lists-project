<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $fillable = [
        'check_list_id',
        'user_id',
        'point_task',
        'is_performed',
    ];
}
