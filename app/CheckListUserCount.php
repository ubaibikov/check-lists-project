<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckListUserCount extends Model
{
    public $primaryKey = 'user_id';

    protected $fillable = [
        'user_id','count',
    ];
}
