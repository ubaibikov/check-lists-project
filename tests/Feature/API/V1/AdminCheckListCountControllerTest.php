<?php

namespace Tests\Feature\API\V1;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminCheckListCountControllerTest extends TestCase
{
    private $baseUrl = 'api/v1/admin/';

    public function testGetCheckListUserCountEmpty()
    {
        $this->withoutMiddleware();

        $user = factory(\App\User::class)->create();

        $response = $this->get($this->baseUrl . "check-lists/count/$user->id");

        $responseData = [
            "message" => 'OK',
            'response'=> [],
        ];

        $response->assertJson($responseData);
    }

    public function testCreateCheckListUserCountCreateSuccess()
    {
        $this->withoutMiddleware();

        $user = factory(\App\User::class)->create();

        $response = $this->post($this->baseUrl . "check-lists/count/$user->id",['count' => 13]);

        $response->assertCreated();
    }

    public function testCreateCheckListUserCountCreateErrorRequired()
    {
        $this->withoutMiddleware();

        $user = factory(\App\User::class)->create();

        $response = $this->post($this->baseUrl . "check-lists/count/$user->id",['count']);

        $response->assertStatus(422);
    }
}
