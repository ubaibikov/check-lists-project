<?php

namespace Tests\Feature\API\V1;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AdminUserControllerTest extends TestCase
{
    use DatabaseMigrations;

    private $baseUrl = 'api/v1/admin/';

    /**
     * CheckResponse
     *
     * @return void
     */
    public function testGetUsersEmptyArray()
    {
        $this->withoutMiddleware();

        $response = $this->get($this->baseUrl . 'users');

        $responseData = [
            "message" => 'OK',
            'response'=> [],
        ];

        $response->assertJson($responseData);
    }

    public function testGetUsers()
    {
        $this->withoutMiddleware();

        $users = factory(\App\User::class)->create();

        $response = $this->get($this->baseUrl . 'users');

        $responseData = [
            "message" => 'OK',
            'response'=> $users->get()->toArray(),
        ];

        $response->assertJson($responseData);
    }

    public function testUserBanSuccess()
    {
        $this->withoutMiddleware();

        $user = factory(\App\User::class)->create();

        $response = $this->patch($this->baseUrl ."users/ban/$user->id",['ban' => 1]);

        $response->assertStatus(200);
    }

    public function testBanErrorMax1()
    {
        $this->withoutMiddleware();

        $user = factory(\App\User::class)->create();

        $response = $this->patch($this->baseUrl ."users/ban/$user->id",['ban' => 12]);

        $response->assertStatus(422);
    }

    public function testBanErrorInteger()
    {
        $this->withoutMiddleware();

        $user = factory(\App\User::class)->create();

        $response = $this->patch($this->baseUrl ."users/ban/$user->id",['ban' => '12']);

        $response->assertStatus(422);
    }

    public function testUserUnbanSuccess()
    {
        $this->withoutMiddleware();

        $user = factory(\App\User::class)->create();

        $response = $this->patch($this->baseUrl ."users/ban/$user->id",['ban' => 0]);

        $response->assertStatus(200);
    }

    public function testUpdateUserSuccess()
    {
        $this->withoutMiddleware();

        $user = factory(\App\User::class)->create();

        $response = $this->patch($this->baseUrl ."users/$user->id",['role' => 'M']);

        $response->assertStatus(200);
    }

    public function testUpdateUserErrorMax1()
    {
        $this->withoutMiddleware();

        $user = factory(\App\User::class)->create();

        $response = $this->patch($this->baseUrl ."users/$user->id",['role' => 'MODE']);
        $response->assertStatus(422);
    }

    public function testUpdateUserErrorNotString()
    {
        $this->withoutMiddleware();

        $user = factory(\App\User::class)->create();

        $response = $this->patch($this->baseUrl ."users/$user->id",['role' => 1]);

        $response->assertStatus(422);
    }
}
